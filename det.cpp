//include <paBezanAshghal>
#include <bits/stdc++.h>
#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<  pii ,long long  >
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-10
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);
typedef long long ll;
typedef long double ld;
using namespace std;
inline ll get_num(char ch){
	if(ch == '-')return 0;
	else if(ch >= 'a' && ch <= 'z'){
		return 1 + (ch-'a');
	}
	else if(ch >= 'A' && ch <= 'Z'){
		return 27 +(ch - 'A');
	}
	else if(ch >= '0' && ch <= '9'){
		return 53 +(ch - '0');
	}
}
//inline ll pw(ll x ,ll y){
//    if(y==0) return 1;
//    ll p = pw(x ,y/2);
//    p*=p;
//    if(y&1) p*=x;
//    return p;
//}
 int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
//int dx[] = {1,0,-1, 0 , 2 ,0 ,-2, 0 , 3 ,0 ,-3 ,0} ,
 //   dy[] = {0,1, 0, -1 , 0 ,2 ,0 , -2 , 0 , 3, 0 , -3};  // 4 Direction

 //int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
    return 0;
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}
    return 0;
}

ll pw(ll x, ll y){
    if(y==0) return 1;
    ll o = y/2;
    ll f = pw(x, o);
    if(y%2){
        return f*f*x;
    }
    else return f*f;
}

const ll MD = 1e9+7;

ll arr[101][101];
    ll n;

ll CNTDET(){
    ll det = 1;
    ll num_rev = 0;
    ll num_mult = 1;
    if(n==1) return arr[0][0];
    Rep(i , n){
        ll ind = i;
        while(ind < n && !arr[ind][i]) ind++;
        if(ind==n) continue;
        if(ind != i)
        {
            Rep(j ,n){
                swap(arr[i][j] , arr[ind][j]);
            }
            num_rev++;
        }
            For(j , i+1 , n){
                ll nmo = arr[i][i];
                ll nmt = arr[j][i];
                if(nmt){
                    Rep(k , n){
                        arr[j][k] = (arr[j][k]*nmo) - (nmt*arr[i][k]);
                    }
                    num_mult*=nmo;
                }
            }
    }
    Rep(i ,n) det*=arr[i][i];
    det*=pw(-1 , num_rev);
    return det/num_mult;
}
int main(){
//    Test;
    cin>>n;
    Rep(i ,n ){
        Rep(j , n){
            cin>>arr[i][j];
        }
    }
    cout<<CNTDET()<<endl;

    return 0;
}
// more information : http://www.algorithmha.ir/
//gauss