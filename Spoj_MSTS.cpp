//include <paBezanAshghal>
#include <bits/stdc++.h>
#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<  long long , pii  >
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Teetout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).reeize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-10
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);
typedef long long ll;
typedef long double ld;
using namespace std;
inline ll get_num(char ch){
    if(ch == '-')return 0;
    else if(ch >= 'a' && ch <= 'z'){
        return 1 + (ch-'a');
    }
    else if(ch >= 'A' && ch <= 'Z'){
        return 27 +(ch - 'A');
    }
    else if(ch >= '0' && ch <= '9'){
        return 53 +(ch - '0');
    }
}
//inline ll pw(ll x ,ll y){
//    if(y==0) return 1;
//    ll p = pw(x ,y/2);
//    p*=p;
//    if(y&1) p*=x;
//    return p;
//}
 int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
//int dx[] = {1,0,-1, 0 , 2 ,0 ,-2, 0 , 3 ,0 ,-3 ,0} ,
 //   dy[] = {0,1, 0, -1 , 0 ,2 ,0 , -2 , 0 , 3, 0 , -3};  // 4 Direction

 //int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
    return 0;
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}
    return 0;
}

ll pw(ll x, ll y){
    if(y==0) return 1;
    ll o = y/2;
    ll f = pw(x, o);
    if(y%2){
        return f*f*x;
    }
    else return f*f;
}

const ll MD = 31011;
#define mod 31011
ll arr[105][105];

    ll dg[105];
//ll n;
vector<piii>v;
vector<pii> qq;
vector<pii> ee;
map<ll ,ll >mp;
ll CNTDET(){
    set<ll>st;
    st.clear();
    mp.clear();
    if(!ee.size() || ee.size()==1) return 1;
  for(auto i : ee)
    {
      st.insert(i.first);
      st.insert(i.second);
    }
  int cnt=st.size();
  cnt--;
  Set(arr, 0);
  ll tt =0;
  for(auto i : st) if(mp.find(i) ==mp.end()) mp[i]= tt++;
  for(int i=0; i<ee.size(); i++)
    {
        ll u = mp[ee[i].F];
        ll v = mp[ee[i].S];
      arr[u][u]++;
      arr[v][v]++;
      arr[u][v]--;
      arr[v][u]--;
    }
    ll ans = 1LL;
    Rep(i , cnt+cnt+2){
        ans*=-1;
    }
    Rep(i , cnt){
        ll ind = i;
        while(ind < cnt && !arr[ind][i]) ind++;
        if(ind == cnt) return 0;
        if(ind != i){
            Rep(k , cnt) swap(arr[i][k] , arr[ind][k]);
            ans*=-1;
        }
        For(j , i+1 , cnt){
            if(arr[j][i]){
                while(true){
                        ll t  =arr[j][i]/arr[i][i];
                        Rep(k , cnt) arr[j][k]-=(arr[i][k]*t) , arr[j][k]%=mod;
                        if(!arr[j][i]) break;
                        Rep(k ,cnt) swap(arr[i][k] , arr[j][k]);
                        ans*=-1;
                }
            }
        }
        ans*=arr[i][i];
        ans%=mod;
    }
    return (ans%mod+mod)%mod;
}
ll p[101];
ll FF(ll x){
    return (p[x]<0) ? x : p[x] = FF(p[x]);
}

int main()
{
  //  Test;
    ll n, m;
  while(~scanf("%lld%lld",&n,&m))
    {
      v.clear();
      memset(p,-1,sizeof(p));
      for(int i=0; i<m; i++)
        {
          ll x,y,z;
          cin>>x>>y>>z;
          x--;
          y--;
          v.push_back(make_pair(z,make_pair(x,y)));
        }
      long long ans=1LL;
      sort(v.begin(),v.end());
      ll l,r=0;
      while(r<v.size())
        {
          l=r++;
          while(r<v.size()&&v[r].first==v[l].first)
            r++;
          for(int i=l; i<r; i++)
            {
              ll& x=v[i].second.first;
              ll& y=v[i].second.second;
              x=FF(x);
              y=FF(y);
            }
          for(int i=l; i<r; i++)
            {
              ll x=v[i].second.first;
              ll y=v[i].second.second;
              x=FF(x);
              y=FF(y);
              if(y!=x)
                p[x]=y;
            }
          for(int i=0; i<n; i++)
            if(p[i]<0)
              {
                for(int j=l; j<r; j++)
                  {
                    ll x=v[j].second.first;
                    ll y=v[j].second.second;
                    if(FF(x)==i&&FF(y)==i)
                      ee.push_back(make_pair(x,y));
                  }
                ans=(ans*CNTDET())%mod;
                                  ee.clear();

              }
        }
      int cot=0;
      for(int i=0; i<n; i++)
        if(p[i]<0)
          cot++;
      if(cot>1)
        {
          puts("0");
          continue;
        }
      printf("%lld\n",(ans%mod+mod)%mod);
    }
    return 0;
}
